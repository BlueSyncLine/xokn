#include <math.h>
#include "graphics.h"
#include "font.h"
#define abs(x) (((x) >= 0) ? (x) : (-(x)))

// Clears the screen, filling it with pixels of the specified color.
void clearScreen(pixel_t *framebuf, pixel_t color) {
    int i;

    for (i = 0; i < SCREEN_WIDTH * SCREEN_HEIGHT; i++)
        framebuf[i] = color;
}

// Sets the value of an individual pixel.
void putPixel(pixel_t *framebuf, int x, int y, pixel_t color) {
    // Check if the coordinates are in bounds.
    if (x >= 0 && y >= 0 && x < SCREEN_WIDTH && y < SCREEN_HEIGHT)
        framebuf[x + y * SCREEN_WIDTH] = color;
}

// Draws a line. [https://en.wikipedia.org/wiki/Digital_differential_analyzer_(graphics_algorithm)]
void drawLine(pixel_t *framebuf, int x1, int y1, int x2, int y2, pixel_t color) {
    int i;
    float x, y, dx, dy, xs, ys, length;

    dx = x2 - x1;
    dy = y2 - y1;

    length = abs(dx) > abs(dy) ? abs(dx) : abs(dy);

    xs = dx / length;
    ys = dy / length;

    x = x1;
    y = y1;

    // Step through the full length and set pixels.
    for (i = 0; i < length; i++) {
        putPixel(framebuf, x, y, color);
        x += xs;
        y += ys;
    }
}

// Draws a filled rectangle.
void fillRect(pixel_t *framebuf, int x, int y, int w, int h, pixel_t color) {
    int i, j;

    for (j = 0; j < h; j++) {
        for (i = 0; i < w; i++)
            putPixel(framebuf, x+i, y+j, color);
    }
}

// Draws a character.
void drawCharacter(pixel_t *framebuf, int row, int col, pixel_t bg, pixel_t fg, char c) {
    int x, y;
    char pixelRow;

    // If the character is outside of the printable range, set it to zero.
    // c is signed, so the check also works for values over 127.
    if (c < ' ')
        c = ' ';

    for (y = 0; y < CHARACTER_HEIGHT; y++) {
        // Account for vertical inter-character spacing (add an extra empty row).
        pixelRow = y < FONT_HEIGHT ? FONT_DATA[(c - ' ') * FONT_HEIGHT + y] : 0x00;
        for (x = 0; x < CHARACTER_WIDTH; x++) {
            // Set the pixel if the bit corresponding to it is set.
            putPixel(framebuf, col * CHARACTER_WIDTH + x, row * CHARACTER_HEIGHT + y, pixelRow & 1 ? fg : bg);

            // Shift the row (there's no need to account for the gap on the right, as the value will be zero anyway).
            pixelRow >>= 1;
        }
    }
}

// Draws a string.
void drawString(pixel_t *framebuf, int row, int col, pixel_t bg, pixel_t fg, const char *s) {
    char c;

    while ((c = *(s++)))
        drawCharacter(framebuf, row, col++, bg, fg, c);
}
