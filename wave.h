// WAV file export rountines.
#ifndef WAVE_H
#define WAVE_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "synth.h"

// "RIFF" magic number
#define RIFF_MAGIC 0x46464952

// "WAVE"
#define RIFF_WAVE 0x45564157

// "fmt "
#define WAVE_SUBCHUNK_FMT 0x20746d66

// "data"
#define WAVE_SUBCHUNK_DATA 0x61746164


// The export format is two 16-bit PCM channels.
#define WAVE_AUDIO_FORMAT 1
#define WAVE_NUM_CHANNELS 2
#define WAVE_SAMPLE_RATE SYNTH_SAMPLE_RATE
#define WAVE_BYTES_PER_SAMPLE 2
#define WAVE_BYTES_PER_FRAME (WAVE_BYTES_PER_SAMPLE * WAVE_NUM_CHANNELS)
#define WAVE_BITS_PER_SAMPLE (WAVE_BYTES_PER_SAMPLE * 8)
#define WAVE_BYTE_RATE (SYNTH_SAMPLE_RATE * WAVE_BYTES_PER_FRAME)
#define WAVE_BLOCK_ALIGN (WAVE_NUM_CHANNELS * WAVE_BYTES_PER_SAMPLE)

typedef struct {
    uint32_t chunkId;
    uint32_t chunkSize;
    uint32_t format;
} riff_header_t;

typedef struct {
    uint32_t subchunkId;
    uint32_t subchunkSize;
} riff_subchunk_header_t;

typedef struct {
    riff_subchunk_header_t subchunkHeader;
    uint16_t audioFormat;
    uint16_t numChannels;
    uint32_t sampleRate;
    uint32_t byteRate;
    uint16_t blockAlign;
    uint16_t bitsPerSample;
} wave_subchunk1_t;

typedef struct {
    riff_subchunk_header_t subchunkHeader;
} wave_subchunk2_t;

typedef struct {
    riff_header_t riffHeader;
    wave_subchunk1_t subchunk1;
    wave_subchunk2_t subchunk2;
} wave_header_t;

typedef int16_t wave_sample_t;

void waveInitHeader(wave_header_t *header, int numFrames);
int waveWriteHeader(FILE *wavfile, wave_header_t *header);
int waveConvertAndWriteSamples(FILE *wavfile, float *samples, int n);

#endif
