all: clean xokn

clean:
	rm -f xokn *.o

xokn: xokn.o
	gcc -o xokn -lm `sdl2-config --libs` xokn.o font.o graphics.o synth.o wave.o

xokn.o: graphics.o synth.o wave.o
	gcc -g -c -o xokn.o -Wall -Wextra `sdl2-config --cflags` main.c

graphics.o: font.o
	gcc -g -c -o graphics.o -Wall -Wextra graphics.c

font.o:
	gcc -g -c -o font.o -Wall -Wextra font.c

synth.o:
	gcc -g -c -o synth.o -Wall -Wextra synth.c

wave.o:
	gcc -g -c -o wave.o -Wall -Wextra wave.c
