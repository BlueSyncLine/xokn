#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "wave.h"
#include "synth.h"

// Initializes a wave_header_t structure.
void waveInitHeader(wave_header_t *header, int numFrames) {
    // Set the fields of the first subchunk.
    header->subchunk1.subchunkHeader.subchunkId = WAVE_SUBCHUNK_FMT;
    header->subchunk1.subchunkHeader.subchunkSize = 16;
    header->subchunk1.audioFormat = WAVE_AUDIO_FORMAT;
    header->subchunk1.numChannels = WAVE_NUM_CHANNELS;
    header->subchunk1.sampleRate = WAVE_SAMPLE_RATE;
    header->subchunk1.byteRate = WAVE_BYTE_RATE;
    header->subchunk1.blockAlign = WAVE_BLOCK_ALIGN;
    header->subchunk1.bitsPerSample = WAVE_BITS_PER_SAMPLE;

    // The second subchunk.
    header->subchunk2.subchunkHeader.subchunkId = WAVE_SUBCHUNK_DATA;
    header->subchunk2.subchunkHeader.subchunkSize = numFrames * WAVE_BYTES_PER_FRAME;

    // The RIFF header.
    header->riffHeader.chunkId = RIFF_MAGIC;
    header->riffHeader.chunkSize = 36 + header->subchunk2.subchunkHeader.subchunkSize;
    header->riffHeader.format = RIFF_WAVE;
}

// Writes the header structure to a file.
// Returns 1 on success, 0 on failure.
int waveWriteHeader(FILE *wavfile, wave_header_t *header) {
    if (!fwrite(header, sizeof(wave_header_t), 1, wavfile))
        return 0;

    return 1;
}

// Converts and writes samples from a synth-format buffer to a file.
// Returns 1 on success, 0 otherwise.
int waveConvertAndWriteSamples(FILE *wavfile, float *samples, int n) {
    int i, j;
    wave_sample_t x;

    for (i = 0; i < n; i++) {
        // Convert the floating-point value to a 16-bit signed int.
        x = (int16_t)(samples[i] * 32767.0f);

        // Repeat the sample according to the number of channels.
        for (j = 0; j < WAVE_NUM_CHANNELS; j++) {
            // If the write fails, return 0.
            if (!fwrite(&x, sizeof(wave_sample_t), 1, wavfile))
                return 0;
        }
    }

    // If the write was successful, return 1.
    return 1;
}
