#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <SDL2/SDL.h>

// For stat
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "synth.h"
#include "graphics.h"
#include "wave.h"

// Flags for waitInput.
#define INPUT_KEYBOARD 1
#define INPUT_MOUSE 2

// The size of the numeric input field, 8 characters.
#define NUMERIC_INPUT_MAX_LENGTH 8

// Key colors
const int KEY_COLORS[OCTAVE_SIZE] = {
    4, 2, 1, 2,
    0, 2, 1, 3,
    0, 2,
    0, 2, 1, 2,
    0, 2, 1, 3,
    0, 2, 1, 3,
    0, 2
};

// The keyboard spans three octaves.
#define KEYBOARD_OCTAVES 3
#define KEYBOARD_OFFSET_ROWS 5
#define KEYBOARD_OFFSET_COLS 4
#define KEYBOARD_WIDTH (KEYBOARD_OCTAVES * OCTAVE_SIZE)
#define KEYBOARD_HEIGHT 3
#define MAX_OCTAVE_OFFSET 5
#define MAX_TONE ((MAX_OCTAVE_OFFSET + KEYBOARD_OCTAVES) * OCTAVE_SIZE - 1)

// Fractional steps for oscillator attack/decay parameters.
#define OSCILLATOR_CONFIG_ATTACK_STEPS 64
#define OSCILLATOR_CONFIG_ATTACK_FRACTION (1.0f / OSCILLATOR_CONFIG_ATTACK_STEPS)

#define OSCILLATOR_CONFIG_DIAGRAM_WIDTH 128
#define OSCILLATOR_CONFIG_DIAGRAM_HEIGHT 64
#define OSCILLATOR_CONFIG_DIAGRAM_OFFSET_X 176
#define OSCILLATOR_CONFIG_DIAGRAM_OFFSET_Y 96
#define OSCILLATOR_CONFIG_DIAGRAM_BORDER 4

// Tab editor parameters

// 22 notes per screen.
#define TAB_EDITOR_SCREEN_SIZE 22
#define TAB_EDITOR_TABLE_STEP 20
#define TAB_EDITOR_TABLE_START_ROW 2
#define TAB_EDITOR_TABLE_OFFSET_COLS 10

// Note preview parameters.
#define PREVIEW_DURATION (SYNTH_SAMPLE_RATE / 4)
#define PREVIEW_ENVELOPE_ATTACK_DECAY 0.75f
#define PREVIEW_ENVELOPE_TRANSITION_SUSTAIN 0.5f
#define PREVIEW_OSCILLATOR OSCILLATOR_SINEMOD

// 256 tabs are allocated.
#define TAB_COUNT 256

// Limited by the way the page numbers are displayed, but should be enough.
#define MAX_TRACK_LENGTH 192

#define ARRANGEMENT_OFFSET_ROWS 5
#define ARRANGEMENT_OFFSET_COLS 4
#define ARRANGEMENT_COLUMNS 24

// File version (to check for mismatches when importing/exporting)
#define XOKN_FILE_VERSION 3

// Hexadecimal values for nibbles.
const char *HEX_NIBBLE = "0123456789abcdef";

char *filename, *exportFilename;
SDL_Renderer *renderer;
SDL_Window *window;
SDL_Texture *texture;
pixel_t *framebuf;
float *previewAudio, *tabAudio;
SDL_AudioSpec audioSpec;
SDL_AudioDeviceID audioDevice;

// Instrument and synth parameters.
synth_params_t synthParams;
int octaveOffset;

// The total track length.
int trackLength;

// The count of loops in the exported WAV file.
int waveLoops;

// The key offset for promptNote.
int keyOffset;

typedef struct {
    int instrumentTabs[SYNTH_INSTRUMENT_COUNT];
} track_section_t;

// Tabs.
int tabsAllocated;
synth_note_t *tabs[TAB_COUNT];
track_section_t *trackSections;

// Redraw the screen.
void redraw() {
    SDL_UpdateTexture(texture, NULL, framebuf, SCREEN_WIDTH * sizeof(pixel_t));
    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, texture, NULL, NULL);
    SDL_RenderPresent(renderer);
}


// Try allocating tabs, return 1 on success, 0 otherwise.
int allocateTabs() {
    int i, j, k;

    for (i = 0; i < TAB_COUNT; i++) {
        if (!(tabs[i] = malloc(synthParams.tabSize * sizeof(synth_note_t))))
            return 0;

        // Initialize the tab to be full of 1-step NOPs.
        for (j = 0; j < synthParams.tabSize; j++) {
            for (k = 0; k < SYNTH_CHORD_SIZE; k++)
                tabs[i][j].tones[k] = -1;

            tabs[i][j].durationSteps = 1;
        }
    }

    tabsAllocated = 1;
    return 1;
}

// Deallocate tabs.
void freeTabs() {
    int i;

    for (i = 0; i < TAB_COUNT; i++)
        free(tabs[i]);
}

// Free memory and exit.
void quit(int exitStatus) {
    SDL_CloseAudioDevice(audioDevice);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    free(framebuf);
    free(previewAudio);
    free(tabAudio);
    free(trackSections);

    // If the tabs were allocated, free them.
    if (tabsAllocated)
        freeTabs();

    exit(exitStatus);
}

// Export the current synth state to a file.
void exportFile() {
    int i, x;
    FILE *f;

    if (!(f = fopen(filename, "w"))) {
        perror("Couldn't open the file for writing");
        quit(EXIT_FAILURE);
    }

    // Store the version.
    x = XOKN_FILE_VERSION;
    if (!fwrite(&x, sizeof(int), 1, f)) {
        perror("Couldn't write the version number");
        fclose(f);
        quit(EXIT_FAILURE);
    }

    // Store the synth parameters.
    if (!fwrite(&synthParams, sizeof(synth_params_t), 1, f)) {
        perror("Couldn't write synthParams");
        fclose(f);
        quit(EXIT_FAILURE);
    }

    // Store the tabs.
    for (i = 0; i < TAB_COUNT; i++) {
        if (fwrite(tabs[i], sizeof(synth_note_t), synthParams.tabSize, f) != (unsigned int)synthParams.tabSize) {
            perror("Couldn't write tab data");
            fclose(f);
            quit(EXIT_FAILURE);
        }
    }

    // Store the track sections.
    if (!fwrite(&trackLength, sizeof(int), 1, f)) {
        perror("Couldn't write the track length");
        fclose(f);
        quit(EXIT_FAILURE);
    }

    for (i = 0; i < trackLength; i++) {
        if (!fwrite(&trackSections[i], sizeof(track_section_t), 1, f)) {
            perror("Couldn't write track section data");
            fclose(f);
            quit(EXIT_FAILURE);
        }
    }

    // Flush and close the file.
    fflush(f);
    fclose(f);
}


// Wait for an input event.
int waitInput(int flags, int *keyboardKeycode, int *mouseX, int *mouseY) {
    SDL_Event event;

    // Infinite loop.
    while (1) {
        // Handle events.
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                // If the user closes the application while we're waiting for a key, quit.
                case SDL_QUIT:
                    quit(EXIT_SUCCESS);
                    break;

                // If the window needs to be redrawn, redraw it.
                case SDL_WINDOWEVENT:
                    if (event.window.event == SDL_WINDOWEVENT_EXPOSED)
                        redraw();
                    break;

                // Keypress.
                case SDL_KEYDOWN:
                    if (flags & INPUT_KEYBOARD) {
                        *keyboardKeycode = event.key.keysym.sym;
                        return INPUT_KEYBOARD;
                    }
                    break;

                // Mouse button press.
                case SDL_MOUSEBUTTONDOWN:
                    if (flags & INPUT_MOUSE) {
                        *mouseX = event.button.x;
                        *mouseY = event.button.y;
                        return INPUT_MOUSE;
                    }
                    break;
            }
        }
    }
}

int waitKey() {
    int keycode;

    // Wait for keyboard input.
    waitInput(INPUT_KEYBOARD, &keycode, NULL, NULL);
    return keycode;
}

// Prompt for numeric input.
int promptNumber(int min, int max) {
    char buf[64], inp[NUMERIC_INPUT_MAX_LENGTH + 1];
    int k, x, cursor;

    snprintf(buf, 64, "Enter a number (range %d-%d)", min, max);
    clearScreen(framebuf, COLOR_LGREY);
    drawString(framebuf, 0, 0, COLOR_WHITE, COLOR_BLACK, buf);
    drawString(framebuf, 1, 0, COLOR_BLUE, COLOR_YELLOW, "        ");
    redraw();

    cursor = 0;
    while (1) {
        k = waitKey();

        if (k == SDLK_RETURN && cursor > 0) {
            // Terminate the input, return.
            inp[cursor] = '\0';

            // If the value is valid, return it.
            x = atoi(inp);
            if (x >= min && x <= max)
                return x;
        } else if (((k >= '0' && k <= '9') || k == '-') && cursor < NUMERIC_INPUT_MAX_LENGTH) {
            // Input a character.
            drawCharacter(framebuf, 1, cursor, COLOR_BLUE, COLOR_YELLOW, k);
            redraw();
            inp[cursor++] = k;
        } else if (k == '\b') {
            // Erase the character.
            drawCharacter(framebuf, 1, cursor > 0 ? cursor - 1 : 0, COLOR_BLUE, COLOR_YELLOW, ' ');
            redraw();

            // If the cursor isn't at the first character, move it back.
            if (cursor)
                cursor--;
        }
    }
}

// Prompt for a hexadecimal tab index.
int promptTabIndex() {
    char k, num[3];
    int i, value;

    clearScreen(framebuf, COLOR_LGREY);
    drawString(framebuf, 0, 0, COLOR_BLUE, COLOR_AQUA, "Enter a tab index (two hexadecimal characters); Escape to cancel.");
    redraw();

    i = 0;
    num[0] = ' ';
    num[1] = ' ';
    num[2] = 0;

    // Wait for the first character.
    while (1) {
        // Draw the number.
        drawCharacter(framebuf, 2, 0, COLOR_BLUE, COLOR_PURPLE, num[0]);
        drawCharacter(framebuf, 2, 1, COLOR_BLUE, COLOR_PURPLE, num[1]);
        redraw();

        k = waitKey();

        // Hex character, add it.
        if (i < 2 && ((k >= '0' && k <= '9') || (k >= 'a' && k <= 'f'))) {
            num[i++] = k;
        } else if (k == '\b' && i > 0) {
            num[--i] = ' ';
        } else if (k == '\r' && i == 2) {
            // Parse and return.
            sscanf(num, "%02x", &value);
            return value;
        } else if (k == SDLK_ESCAPE) {
            // Cancel.
            return -1;
        }
    }
}

// Play a preview of a tone.
void previewTone(instrument_params_t instrument, int tone) {
    int i;
    float t, frequency;

    frequency = toneToFrequency(tone);

    // Generate the preview audio.
    for (i = 0; i < PREVIEW_DURATION; i++) {
        // The percentage into the envelope.
        t = (float)i / PREVIEW_DURATION;

        previewAudio[i] = oscillatorFunction(instrument.oscillatorType, frequency, i) *
            envelope(instrument.attackDecay, instrument.transitionSustain, instrument.envelopeDuration, t) * instrument.volume;
    }

    // Play the preview audio.
    SDL_ClearQueuedAudio(audioDevice);
    SDL_QueueAudio(audioDevice, previewAudio, sizeof(float) * PREVIEW_DURATION);
}

// Play a truncated, modified preview of a tab (using previewInstrument as the instrument).
void previewTab(synth_note_t *tab, int maxLength, int previewInstrument) {
    int i;
    synth_params_t modifiedParams;
    instrument_params_t dummyInstrument;
    synth_note_t *modifiedTabs[SYNTH_INSTRUMENT_COUNT];

    // If the maximum length is not specified, use the full tab length.
    if (!maxLength)
        maxLength = synthParams.tabSize;

    // Dummy instrument to fill the missing slots.
    dummyInstrument.oscillatorType = 0;
    dummyInstrument.attackDecay = 0.0f;
    dummyInstrument.transitionSustain = 0.0f;
    dummyInstrument.envelopeDuration = 0.0f;
    dummyInstrument.volume = 0.0f;

    modifiedParams = synthParams;
    modifiedParams.tabSize = maxLength;

    // Pad the rest.
    for (i = 0; i < SYNTH_INSTRUMENT_COUNT; i++) {
        // As the first instrument we'll use the real instrument chosen for the preview.
        modifiedParams.instruments[i] = i == 0 ? synthParams.instruments[previewInstrument] : dummyInstrument;

        // Use the same tab for all instruments (including the dummy ones).
        modifiedTabs[i] = tab;
    }

    // Synthesize audio.
    synthesizeTabGroup(tabAudio, modifiedParams, (const synth_note_t**)modifiedTabs);

    // Play it (truncated, of course).
    SDL_ClearQueuedAudio(audioDevice);
    SDL_QueueAudio(audioDevice, tabAudio, sizeof(float) * tempoToSamples(synthParams.tempo) * maxLength);
}

// Plays a complete tab from the track.
void playFullTab(int trackPosition) {
    int i;
    synth_note_t *tabGroup[SYNTH_INSTRUMENT_COUNT];

    // Prepare the array of pointers to tabs for synthesizeTabGroup.
    for (i = 0; i < SYNTH_INSTRUMENT_COUNT; i++)
        tabGroup[i] = tabs[trackSections[trackPosition].instrumentTabs[i]];

    // Synthesize audio.
    synthesizeTabGroup(tabAudio, synthParams, (const synth_note_t**)tabGroup);

    // And play it.
    SDL_ClearQueuedAudio(audioDevice);
    SDL_QueueAudio(audioDevice, tabAudio, sizeof(float) * tabDuration(synthParams));
}

// Draw the clickable keyboard for promptNote.
void drawKeyboard(int currentNote, int previewInstrument) {
    int i, j, keyColor, stagger;
    pixel_t background, foreground;
    unsigned int k;
    const char *noteName;

    clearScreen(framebuf, COLOR_LGREY);
    drawString(framebuf, 0, 0, COLOR_BLUE, COLOR_AQUA, "Choose a note: ");
    drawString(framebuf, 0, 16, COLOR_BLUE, COLOR_YELLOW, "  ");
    drawString(framebuf, 0, 16, COLOR_BLUE, COLOR_YELLOW, NOTE_NAMES[currentNote % OCTAVE_SIZE]);

    // Adding an ASCII zero is used as a hack for quickly converting a value to char.
    drawCharacter(framebuf, 0, 19, COLOR_BLUE, COLOR_RED, '0' + currentNote / OCTAVE_SIZE);

    drawString(framebuf, 2, 0, COLOR_BLUE, COLOR_AQUA, "Octave offset (q/w): ");
    drawCharacter(framebuf, 2, 22, COLOR_BLUE, COLOR_RED, '0' + octaveOffset);

    drawString(framebuf, 3, 0, COLOR_BLUE, COLOR_AQUA, "Preview instrument: ");
    drawCharacter(framebuf, 3, 22, COLOR_BLUE, COLOR_RED, '0' + previewInstrument + 1);

    for (i = 0; i < KEYBOARD_OCTAVES; i++) {
        for (j = 0; j < OCTAVE_SIZE; j++) {
            noteName = NOTE_NAMES[j];
            keyColor = KEY_COLORS[(j + OCTAVE_SIZE - keyOffset) % OCTAVE_SIZE];

            // Select the colors for the key drawn.
            switch (keyColor) {
                case 0:
                    // White piano keys (whole tones).
                    background = COLOR_WHITE;
                    foreground = COLOR_BLACK;
                    stagger = 0;
                    break;
                case 1:
                    // Black piano keys.
                    background = COLOR_BLACK;
                    foreground = COLOR_WHITE;
                    stagger = 0;
                    break;
                case 2:
                    // Microtonal (quarter-tone) keys.
                    background = COLOR_GREY;
                    foreground = COLOR_BLACK;
                    stagger = 1;
                    break;
                case 3:
                    // C "neutral" grey keys.
                    background = COLOR_BLUE;
                    foreground = COLOR_YELLOW;
                    stagger = 1;
                    break;
                case 4:
                    // Tonic.
                    background = COLOR_WHITE;
                    foreground = COLOR_RED;
                    stagger = 0;
                    break;
            }

            // Draw the note name (pad to three or two characters).
            for (k = 0; k < (stagger ? 2 : 3); k++)
                drawCharacter(framebuf, KEYBOARD_OFFSET_ROWS + k + (stagger ? 1 : 0), i * OCTAVE_SIZE + j + KEYBOARD_OFFSET_COLS, background, foreground, k < strlen(noteName) ? noteName[k] : ' ');
        }
    }

    redraw();
}

// Ask the user to choose a note.
int promptNote(int previewInstrument) {
    int n, inputType, keycode, mouseX, mouseY, col, row;

    // By default, the note chosen is C of the last octave offset.
    n = octaveOffset * OCTAVE_SIZE;

    while (1) {
        drawKeyboard(n, previewInstrument);

        inputType = waitInput(INPUT_KEYBOARD | INPUT_MOUSE, &keycode, &mouseX, &mouseY);

        switch (inputType) {
            case INPUT_KEYBOARD:
                switch (keycode) {
                    // Change octave offset.
                    case 'q':
                    case 'w':
                        if (keycode == 'q' && octaveOffset > 0)
                            octaveOffset--;
                        else if (keycode == 'w' && octaveOffset < MAX_OCTAVE_OFFSET)
                            octaveOffset++;

                        break;

                    // Change the key offset.
                    case 'k':
                        keyOffset = n % OCTAVE_SIZE;
                        break;

                    // Enter leaves the note selection screen.
                    case SDLK_RETURN:
                        return n;
                }
                break;

            case INPUT_MOUSE:
                // Convert the mouse coordinates into a note value.
                col = mouseX / CHARACTER_WIDTH;
                row = mouseY / CHARACTER_HEIGHT;

                // If the column is in range, use it as the note value.
                if (col >= KEYBOARD_OFFSET_COLS && col < KEYBOARD_OFFSET_COLS + KEYBOARD_WIDTH && row >= KEYBOARD_OFFSET_ROWS && row < KEYBOARD_OFFSET_ROWS + KEYBOARD_HEIGHT) {
                    n = col - KEYBOARD_OFFSET_COLS + octaveOffset * OCTAVE_SIZE;

                    // Play a preview.
                    previewTone(synthParams.instruments[previewInstrument], n);
                }

                break;
        }
    }
}

// Draw a diagram showing the envelope specified by the parameters given.
void drawAttackSustainConfiguration(int attackDecaySteps, int transitionSustainSteps, int envelopeDurationSteps, int volumeSteps) {
    int envelope, transition, sustain, attack, peakY;

    fillRect(framebuf, OSCILLATOR_CONFIG_DIAGRAM_OFFSET_X - OSCILLATOR_CONFIG_DIAGRAM_BORDER, OSCILLATOR_CONFIG_DIAGRAM_OFFSET_Y - OSCILLATOR_CONFIG_DIAGRAM_BORDER,
                OSCILLATOR_CONFIG_DIAGRAM_WIDTH + OSCILLATOR_CONFIG_DIAGRAM_BORDER * 2, OSCILLATOR_CONFIG_DIAGRAM_HEIGHT + OSCILLATOR_CONFIG_DIAGRAM_BORDER * 2, COLOR_WHITE);

    envelope = OSCILLATOR_CONFIG_DIAGRAM_WIDTH * envelopeDurationSteps / OSCILLATOR_CONFIG_ATTACK_STEPS;
    transition = envelope * transitionSustainSteps / OSCILLATOR_CONFIG_ATTACK_STEPS;
    attack = transition * attackDecaySteps / OSCILLATOR_CONFIG_ATTACK_STEPS;
    sustain = envelope - transition;

    peakY = OSCILLATOR_CONFIG_DIAGRAM_OFFSET_Y + (OSCILLATOR_CONFIG_DIAGRAM_HEIGHT * (OSCILLATOR_CONFIG_ATTACK_STEPS - volumeSteps) / OSCILLATOR_CONFIG_ATTACK_STEPS);

    // Attack
    drawLine(framebuf,
        OSCILLATOR_CONFIG_DIAGRAM_OFFSET_X, OSCILLATOR_CONFIG_DIAGRAM_OFFSET_Y + OSCILLATOR_CONFIG_DIAGRAM_HEIGHT,
        OSCILLATOR_CONFIG_DIAGRAM_OFFSET_X + attack, peakY, COLOR_RED);

    // Sustain
    drawLine(framebuf,
        OSCILLATOR_CONFIG_DIAGRAM_OFFSET_X + attack, peakY,
        OSCILLATOR_CONFIG_DIAGRAM_OFFSET_X + attack + sustain, peakY, COLOR_GREEN);

    // Decay
    drawLine(framebuf,
        OSCILLATOR_CONFIG_DIAGRAM_OFFSET_X + attack + sustain, peakY,
        OSCILLATOR_CONFIG_DIAGRAM_OFFSET_X + envelope, OSCILLATOR_CONFIG_DIAGRAM_OFFSET_Y + OSCILLATOR_CONFIG_DIAGRAM_HEIGHT, COLOR_BLUE);

    // Zero
    drawLine(framebuf,
        OSCILLATOR_CONFIG_DIAGRAM_OFFSET_X + envelope, OSCILLATOR_CONFIG_DIAGRAM_OFFSET_Y + OSCILLATOR_CONFIG_DIAGRAM_HEIGHT,
        OSCILLATOR_CONFIG_DIAGRAM_OFFSET_X + OSCILLATOR_CONFIG_DIAGRAM_WIDTH, OSCILLATOR_CONFIG_DIAGRAM_OFFSET_Y + OSCILLATOR_CONFIG_DIAGRAM_HEIGHT, COLOR_BLACK);
}

// Draw the envelope data.
void drawEnvelope(int attackDecaySteps, int transitionSustainSteps, int durationSteps, int volumeSteps) {
    // Draw the numbers as two-digit values.
    drawCharacter(framebuf, 8, 24, COLOR_BLUE, COLOR_GREEN, '0' + attackDecaySteps / 10);
    drawCharacter(framebuf, 8, 25, COLOR_BLUE, COLOR_GREEN, '0' + attackDecaySteps % 10);

    drawCharacter(framebuf, 9, 24, COLOR_BLUE, COLOR_GREEN, '0' + transitionSustainSteps / 10);
    drawCharacter(framebuf, 9, 25, COLOR_BLUE, COLOR_GREEN, '0' + transitionSustainSteps % 10);

    drawCharacter(framebuf, 10, 24, COLOR_BLUE, COLOR_GREEN, '0' + durationSteps / 10);
    drawCharacter(framebuf, 10, 25, COLOR_BLUE, COLOR_GREEN, '0' + durationSteps % 10);

    drawCharacter(framebuf, 11, 24, COLOR_BLUE, COLOR_GREEN, '0' + volumeSteps / 10);
    drawCharacter(framebuf, 11, 25, COLOR_BLUE, COLOR_GREEN, '0' + volumeSteps % 10);

    drawAttackSustainConfiguration(attackDecaySteps, transitionSustainSteps, durationSteps, volumeSteps);
    redraw();
}

// Show a dialog for configuring an oscillator.
void instrumentConfiguration(instrument_params_t *config) {
    int k, attackDecaySteps, transitionSustainSteps, durationSteps, volumeSteps;
    instrument_params_t previewConfig;

    // Use the default values from the configuration first.
    attackDecaySteps = config->attackDecay / OSCILLATOR_CONFIG_ATTACK_FRACTION;
    transitionSustainSteps = config->transitionSustain / OSCILLATOR_CONFIG_ATTACK_FRACTION;
    durationSteps = config->envelopeDuration / OSCILLATOR_CONFIG_ATTACK_FRACTION;
    volumeSteps = config->volume / OSCILLATOR_CONFIG_ATTACK_FRACTION;

    clearScreen(framebuf, COLOR_LGREY);
    drawString(framebuf, 0, 0, COLOR_BLUE, COLOR_AQUA, "Oscillator configuration");
    drawString(framebuf, 2, 1, COLOR_BLUE, COLOR_AQUA, "Oscillator type (t): ");
    drawString(framebuf, 2, 23, COLOR_BLUE, COLOR_GREEN, "          ");
    drawString(framebuf, 2, 23, COLOR_BLUE, COLOR_GREEN, OSCILLATOR_NAMES[config->oscillatorType]);
    drawString(framebuf, 5, 1, COLOR_BLUE, COLOR_YELLOW, "q/w - attack/decay, a/s - transition/sustain, e/r - duration, z/x - volume");
    drawString(framebuf, 6, 1, COLOR_BLUE, COLOR_YELLOW, "Press p to preview and Enter to continue.");
    drawString(framebuf, 8, 1, COLOR_BLUE, COLOR_AQUA,  "Attack / Decay......: ");
    drawString(framebuf, 9, 1, COLOR_BLUE, COLOR_AQUA,  "Transition / Sustain: ");
    drawString(framebuf, 10, 1, COLOR_BLUE, COLOR_AQUA, "Envelope Duration...: ");
    drawString(framebuf, 11, 1, COLOR_BLUE, COLOR_AQUA, "Channel Volume......: ");

    // Draw the envelope parameters.
    drawEnvelope(attackDecaySteps, transitionSustainSteps, durationSteps, volumeSteps);

    while (1) {
        switch (k = waitKey()) {
            case 't':
                config->oscillatorType = (config->oscillatorType + 1) % OSCILLATOR_COUNT;
                drawString(framebuf, 2, 23, COLOR_BLUE, COLOR_GREEN, "          ");
                drawString(framebuf, 2, 23, COLOR_BLUE, COLOR_GREEN, OSCILLATOR_NAMES[config->oscillatorType]);
                redraw();
                break;
            case 'p':
                // Play a preview tone.
                previewConfig = *config;
                previewConfig.attackDecay = attackDecaySteps * OSCILLATOR_CONFIG_ATTACK_FRACTION;
                previewConfig.transitionSustain = transitionSustainSteps * OSCILLATOR_CONFIG_ATTACK_FRACTION;
                previewConfig.envelopeDuration = durationSteps * OSCILLATOR_CONFIG_ATTACK_FRACTION;
                previewConfig.volume = volumeSteps * OSCILLATOR_CONFIG_ATTACK_FRACTION;
                previewTone(previewConfig, TONE_A4);
                break;

            case 'q':
            case 'w':
            case 'a':
            case 's':
            case 'z':
            case 'x':
            case 'e':
            case 'r':
                if (k == 'q' && attackDecaySteps > 0)
                    attackDecaySteps--;
                else if (k == 'w' && attackDecaySteps < OSCILLATOR_CONFIG_ATTACK_STEPS)
                    attackDecaySteps++;
                else if (k == 'a' && transitionSustainSteps > 0)
                    transitionSustainSteps--;
                else if (k == 's' && transitionSustainSteps < OSCILLATOR_CONFIG_ATTACK_STEPS)
                    transitionSustainSteps++;
                else if (k == 'z' && volumeSteps > 0)
                    volumeSteps--;
                else if (k == 'x' && volumeSteps < OSCILLATOR_CONFIG_ATTACK_STEPS)
                    volumeSteps++;
                else if (k == 'e' && durationSteps > 0)
                    durationSteps--;
                else if (k == 'r' && durationSteps < OSCILLATOR_CONFIG_ATTACK_STEPS)
                    durationSteps++;

                // Update the displayed data.
                drawEnvelope(attackDecaySteps, transitionSustainSteps, durationSteps, volumeSteps);
                break;

            case SDLK_RETURN:
                // Store the configuration.
                config->attackDecay = attackDecaySteps * OSCILLATOR_CONFIG_ATTACK_FRACTION;
                config->transitionSustain = transitionSustainSteps * OSCILLATOR_CONFIG_ATTACK_FRACTION;
                config->envelopeDuration = durationSteps * OSCILLATOR_CONFIG_ATTACK_FRACTION;
                config->volume = volumeSteps * OSCILLATOR_CONFIG_ATTACK_FRACTION;
                return;
        }
    }
}

// Help screen.
void tabEditorHelp() {
    clearScreen(framebuf, COLOR_LGREY);
    drawString(framebuf, 0, 0, COLOR_GREY, COLOR_YELLOW, "xokn help - tab editor");
    drawString(framebuf, 2, 0, COLOR_GREY, COLOR_BLACK, "Keys");

    drawString(framebuf, 3, 0, COLOR_BLUE, COLOR_WHITE, "p");
    drawString(framebuf, 3, 10, COLOR_LGREY, COLOR_BLACK, "Preview tab");

    drawString(framebuf, 4, 0, COLOR_BLUE, COLOR_WHITE, "i");
    drawString(framebuf, 4, 10, COLOR_LGREY, COLOR_BLACK, "Insert tab");

    drawString(framebuf, 5, 0, COLOR_BLUE, COLOR_WHITE, "z");
    drawString(framebuf, 5, 10, COLOR_LGREY, COLOR_BLACK, "Remove tone");

    drawString(framebuf, 6, 0, COLOR_BLUE, COLOR_WHITE, "[");
    drawString(framebuf, 6, 10, COLOR_LGREY, COLOR_BLACK, "Decrement note duration");

    drawString(framebuf, 7, 0, COLOR_BLUE, COLOR_WHITE, "]");
    drawString(framebuf, 7, 10, COLOR_LGREY, COLOR_BLACK, "Increment note duration");

    drawString(framebuf, 8, 0, COLOR_BLUE, COLOR_WHITE, ".");
    drawString(framebuf, 8, 10, COLOR_LGREY, COLOR_BLACK, "Transpose tab");

    drawString(framebuf, 9, 0, COLOR_BLUE, COLOR_WHITE, "k");
    drawString(framebuf, 9, 10, COLOR_LGREY, COLOR_BLACK, "Modify current tone");

    drawString(framebuf, 10, 0, COLOR_BLUE, COLOR_WHITE, ";");
    drawString(framebuf, 10, 10, COLOR_LGREY, COLOR_BLACK, "Insert NOP, shift down");

    drawString(framebuf, 11, 0, COLOR_BLUE, COLOR_WHITE, "'");
    drawString(framebuf, 11, 10, COLOR_LGREY, COLOR_BLACK, "Remove current note, shift up");

    drawString(framebuf, 12, 0, COLOR_BLUE, COLOR_WHITE, "Space");
    drawString(framebuf, 12, 10, COLOR_LGREY, COLOR_BLACK, "Pick tone");

    drawString(framebuf, 13, 0, COLOR_BLUE, COLOR_WHITE, "Enter");
    drawString(framebuf, 13, 10, COLOR_LGREY, COLOR_BLACK, "To arrangement screen");

    // Redraw, applying the above.
    redraw();

    // Wait for a keypress.
    waitKey();
}

// Draw the tab editor screen.
void tabEditorDraw(synth_note_t *tab, int tabOffset, int cursorPosition, int cursorIndex) {
    int i, j, tabIndex, tone, paddingNotes;
    char buf[3];
    pixel_t fg, bg;

    clearScreen(framebuf, COLOR_LGREY);
    drawString(framebuf, 0, 0, COLOR_BLUE, COLOR_AQUA, "Tab editor (press F1 for help)");

    paddingNotes = 0;

    // Find the initial count of padding notes preceding the visible part of the tab.
    for (i = 0; i < tabOffset; i++) {
        if (!paddingNotes)
            paddingNotes = tab[i].durationSteps - 1;
        else
            paddingNotes--;
    }

    for (i = 0; i < TAB_EDITOR_SCREEN_SIZE; i++) {
        tabIndex = i + tabOffset;

        // Draw the tab index.
        snprintf(buf, 3, "%02d", tabIndex);
        drawString(framebuf, TAB_EDITOR_TABLE_START_ROW + i, 0, COLOR_BLUE, COLOR_RED, buf);

        for (j = 0; j < SYNTH_CHORD_SIZE; j++) {
            bg = i == cursorPosition && j == cursorIndex ? COLOR_WHITE : COLOR_BLUE;
            fg = i == cursorPosition && j == cursorIndex ? COLOR_BLACK : COLOR_YELLOW;

            // If the tab index is out of bounds, draw padding.
            if (tabIndex >= synthParams.tabSize) {
                drawString(framebuf, TAB_EDITOR_TABLE_START_ROW + i, TAB_EDITOR_TABLE_OFFSET_COLS + j * TAB_EDITOR_TABLE_STEP, bg, COLOR_GREY, "...");
            } else {
                // Otherwise, draw the note name and octave.
                if (!paddingNotes) {
                    tone = tab[tabIndex].tones[j];

                    if (tone != -1) {
                        // 24-TET tone.
                        drawString(framebuf, TAB_EDITOR_TABLE_START_ROW + i, TAB_EDITOR_TABLE_OFFSET_COLS + j * TAB_EDITOR_TABLE_STEP, bg, fg, "  ");
                        drawString(framebuf, TAB_EDITOR_TABLE_START_ROW + i, TAB_EDITOR_TABLE_OFFSET_COLS + j * TAB_EDITOR_TABLE_STEP, bg, fg, NOTE_NAMES[tone % OCTAVE_SIZE]);
                        drawCharacter(framebuf, TAB_EDITOR_TABLE_START_ROW + i, TAB_EDITOR_TABLE_OFFSET_COLS + j * TAB_EDITOR_TABLE_STEP + 2, bg, COLOR_RED, '0' + (tone / OCTAVE_SIZE));
                    } else {
                        // Empty note.
                        drawString(framebuf, TAB_EDITOR_TABLE_START_ROW + i, TAB_EDITOR_TABLE_OFFSET_COLS + j * TAB_EDITOR_TABLE_STEP, bg, COLOR_GREEN, "nop");
                    }

                } else {
                    // Note continuation padding.
                    drawString(framebuf, TAB_EDITOR_TABLE_START_ROW + i, TAB_EDITOR_TABLE_OFFSET_COLS + j * TAB_EDITOR_TABLE_STEP, bg, COLOR_GREY, "^  ");
                }
            }
        }

        // Find the amount of padding notes.
        if (paddingNotes > 0) {
            paddingNotes--;
        } else if (tabIndex < synthParams.tabSize) {
            paddingNotes = tab[tabIndex].durationSteps - 1;
        }
    }

    redraw();
}

// Copies a source tab into a destination one.
void copyTab(synth_note_t *destination, synth_note_t *source) {
    int i;

    for (i = 0; i < synthParams.tabSize; i++)
        destination[i] = source[i];
}

// Show the tab editing menu.
void tabEditor(synth_note_t *tab, int previewInstrument) {
    int i, j, k, tabOffset, cursorPosition, cursorIndex, tabIndex, newTone, toneOffset, copyTabIndex;

    // Initialize the variables.
    tabOffset = 0;
    cursorPosition = 0;
    cursorIndex = 0;

    while (1) {
        tabIndex = tabOffset + cursorPosition;
        tabEditorDraw(tab, tabOffset, cursorPosition, cursorIndex);

        k = waitKey();
        if (k == SDLK_F1) {
            // Invoke the tab editor help screen.
            tabEditorHelp();
        } else if (k == SDLK_UP) {
            // Simply move the cursor up.
            if (cursorPosition > 0) {
                cursorPosition--;
            } else if (tabOffset > 0) {
                // Page up otherwise.
                cursorPosition = TAB_EDITOR_SCREEN_SIZE - 1;
                tabOffset -= TAB_EDITOR_SCREEN_SIZE;
            }
        } else if (k == SDLK_DOWN) {
            // The cursor can be moved down.
            if (cursorPosition < TAB_EDITOR_SCREEN_SIZE - 1) {
                if (tabOffset + cursorPosition < synthParams.tabSize - 1)
                    cursorPosition++;
            } else if (tabOffset + cursorPosition < synthParams.tabSize - 1) {
                // Page down.
                cursorPosition = 0;
                tabOffset += TAB_EDITOR_SCREEN_SIZE;
            }

        } else if (k == SDLK_LEFT && cursorIndex > 0) {
            // Cursor to the previous channel.
            cursorIndex--;
        } else if (k == SDLK_RIGHT && cursorIndex < SYNTH_CHORD_SIZE - 1) {
            // Next channel.
            cursorIndex++;
        } else if (k == SDLK_RETURN) {
            // Quit.
            return;
        } else if (k == 'p') {
            // Preview tab.
            previewTab(tab, tabIndex + 1, previewInstrument);
        } else if (k == 'i') {
            // Copy another tab into the current one.
            if ((copyTabIndex = promptTabIndex()) != -1)
                copyTab(tab, tabs[copyTabIndex]);
        } else if (k == 'z') {
            // Zero out the selected note.
            tab[tabIndex].tones[cursorIndex] = -1;
        } else if (k == ' ') {
            // Select a note for the current tab index.
            tab[tabIndex].tones[cursorIndex] = promptNote(previewInstrument);
        } else if (k == '[' && tab[tabIndex].durationSteps > 1) {
            // Decrement the note duration.
            tab[tabIndex].durationSteps--;
        } else if (k == ']' && tab[tabIndex].durationSteps < synthParams.tabSize - tabIndex) {
            // Increment the note duration.
            tab[tabIndex].durationSteps++;
        } else if (k == '.') {
            // Shift notes.
            toneOffset = promptNumber(-OCTAVE_SIZE * 2, OCTAVE_SIZE * 2);
            for (i = 0; i < synthParams.tabSize; i++) {
                for (j = 0; j < SYNTH_CHORD_SIZE; j++) {
                    // If the initial tone is not a NOP, apply the offset.
                    if (tab[i].tones[j] >= 0) {
                        newTone = tab[i].tones[j] + toneOffset;

                        // If the new tone would still be in range, apply; replace with a NOP otherwise.
                        tab[i].tones[j] = (newTone >= 0 && newTone <= MAX_TONE) ? newTone : -1;
                    }
                }
            }
        } else if (k == ';' && tabIndex < synthParams.tabSize - 1) {
            // Insert NOP and shift the rest of the notes down.
            for (i = synthParams.tabSize - 1; i >= tabIndex + 1; i--)
                tab[i] = tab[i - 1];

            // Insert a NOP at the split point.
            for (i = 0; i < SYNTH_CHORD_SIZE; i++)
                tab[tabIndex].tones[i] = -1;
        } else if (k == '\'' && tabIndex < synthParams.tabSize - 1) {
            // The opposite of the above; remove the current note and shift the others below it up.
            for (i = tabIndex; i < synthParams.tabSize - 1; i++)
                tab[i] = tab[i + 1];

            // The last note becomes a NOP (since there's nothing to copy up).
            for (i = 0; i < SYNTH_CHORD_SIZE; i++)
                tab[synthParams.tabSize - 1].tones[i] = -1;
        }
    }
}

// Arrangement screen help.
void arrangementHelp() {
    clearScreen(framebuf, COLOR_LGREY);
    drawString(framebuf, 0, 0, COLOR_GREY, COLOR_YELLOW, "xokn help - arrangement screen");
    drawString(framebuf, 2, 0, COLOR_GREY, COLOR_BLACK, "Keys");

    drawString(framebuf, 3, 0, COLOR_BLUE, COLOR_WHITE, "1-4");
    drawString(framebuf, 3, 10, COLOR_LGREY, COLOR_BLACK, "Configure instrument channel");
    drawString(framebuf, 4, 0, COLOR_BLUE, COLOR_WHITE, "Space");
    drawString(framebuf, 4, 10, COLOR_LGREY, COLOR_BLACK, "Change tab index");
    drawString(framebuf, 5, 0, COLOR_BLUE, COLOR_WHITE, "[ / ]");
    drawString(framebuf, 5, 10, COLOR_LGREY, COLOR_BLACK, "Scroll (page)");
    drawString(framebuf, 6, 0, COLOR_BLUE, COLOR_WHITE, ";");
    drawString(framebuf, 6, 10, COLOR_LGREY, COLOR_BLACK, "Shift tab groups left");
    drawString(framebuf, 7, 0, COLOR_BLUE, COLOR_WHITE, "'");
    drawString(framebuf, 7, 10, COLOR_LGREY, COLOR_BLACK, "Shift tab groups right, insert empty space");
    drawString(framebuf, 8, 0, COLOR_BLUE, COLOR_WHITE, "=");
    drawString(framebuf, 8, 10, COLOR_LGREY, COLOR_BLACK, "Change track length");
    drawString(framebuf, 9, 0, COLOR_BLUE, COLOR_WHITE, "p");
    drawString(framebuf, 9, 10, COLOR_LGREY, COLOR_BLACK, "Preview current tab");
    drawString(framebuf, 10, 0, COLOR_BLUE, COLOR_WHITE, "`");
    drawString(framebuf, 10, 10, COLOR_LGREY, COLOR_BLACK, "Preview score");
    drawString(framebuf, 11, 0, COLOR_BLUE, COLOR_WHITE, "Enter");
    drawString(framebuf, 11, 10, COLOR_LGREY, COLOR_BLACK, "Edit tab");
    drawString(framebuf, 12, 0, COLOR_BLUE, COLOR_WHITE, "Escape");
    drawString(framebuf, 12, 10, COLOR_LGREY, COLOR_BLACK, "Exit (show save prompt)");

    // Redraw.
    redraw();

    // Wait for a key.
    waitKey();
}

// Draw the arrangement screen.
void arrangementDraw(int cursorInstrument, int cursorPosition, int playbackPosition, int page, int pageCount) {
    int i, j, trackIndex, tabNum;
    pixel_t fg, bg;
    float duration;
    int durationMinutes, durationSeconds;
    char buf[64];

    clearScreen(framebuf, COLOR_LGREY);
    drawString(framebuf, 0, 0, COLOR_BLUE, COLOR_AQUA, "Arrangement (press F1 for help)");
    drawString(framebuf, 2, 0, COLOR_BLUE, COLOR_AQUA, "Page: ");

    // This assumes that there can be up to 10 pages.
    drawCharacter(framebuf, 2, 7, COLOR_BLUE, COLOR_PURPLE, '0' + page + 1);
    drawCharacter(framebuf, 2, 9, COLOR_BLUE, COLOR_AQUA, 'o');
    drawCharacter(framebuf, 2, 10, COLOR_BLUE, COLOR_AQUA, 'f');
    drawCharacter(framebuf, 2, 12, COLOR_BLUE, COLOR_PURPLE, '0' + (pageCount - 1) + 1);

    // Draw instrument indices.
    for (j = 0; j < SYNTH_INSTRUMENT_COUNT; j++)
        drawCharacter(framebuf, ARRANGEMENT_OFFSET_ROWS + j, 0, COLOR_BLUE, COLOR_RED, '1' + j);


    for (i = 0; i < ARRANGEMENT_COLUMNS; i++) {
        for (j = 0; j < SYNTH_INSTRUMENT_COUNT; j++) {
            // Highlight the cursor.
            if (j == cursorInstrument && i == cursorPosition) {
                fg = COLOR_RED;
                bg = COLOR_WHITE;
            } else {
                fg = COLOR_YELLOW;
                bg = COLOR_BLACK;
            }

            // Find the index into the tab layout.
            trackIndex = page * ARRANGEMENT_COLUMNS + i;

            // Playback highlight.
            if (trackIndex == playbackPosition)
                bg = COLOR_GREY;

            // If it's in range, it can be used as an index.
            if (trackIndex < trackLength) {
                // Draw the hexadecimal character.
                tabNum = trackSections[trackIndex].instrumentTabs[j];
                drawCharacter(framebuf, ARRANGEMENT_OFFSET_ROWS + j, ARRANGEMENT_OFFSET_COLS + i * 3, bg, fg, HEX_NIBBLE[tabNum >> 4]);
                drawCharacter(framebuf, ARRANGEMENT_OFFSET_ROWS + j, ARRANGEMENT_OFFSET_COLS + i * 3 + 1, bg, fg, HEX_NIBBLE[tabNum & 0xf]);
            }
        }
    }

    // Compute and draw the track length.
    duration = synthParams.tabSize * trackLength / (float)synthParams.tempo;
    durationMinutes = duration;
    durationSeconds = fmod(duration, 1.0f) * 60.0f;

    snprintf(buf, 64, "Cursor position: %03d/%03d", cursorPosition + page * ARRANGEMENT_COLUMNS, trackLength);
    drawString(framebuf, 21, 0, COLOR_GREY, COLOR_BLACK, buf);

    snprintf(buf, 64, "Track length [=]: %d tabs", trackLength);
    drawString(framebuf, 23, 0, COLOR_GREY, COLOR_BLACK, buf);

    snprintf(buf, 64, "Track duration: %02d:%02d", durationMinutes, durationSeconds);
    drawString(framebuf, 24, 0, COLOR_GREY, COLOR_BLACK, buf);

    redraw();
}

// Show an overall arrangement screen.
void arrangementScreen() {
    int k, i, finished, cursorInstrument, cursorPosition, page, tabIndex, pageCount, cursorTrackPosition;

    cursorInstrument = 0;
    cursorPosition = 0;
    page = 0;

    while (1) {
        // Update the page count.
        pageCount = trackLength / ARRANGEMENT_COLUMNS + (trackLength % ARRANGEMENT_COLUMNS ? 1 : 0);

        // The full cursor position.
        cursorTrackPosition = cursorPosition + page * ARRANGEMENT_COLUMNS;

        // Redraw the screen.
        arrangementDraw(cursorInstrument, cursorPosition, -1, page, pageCount);

        switch (k = waitKey()) {
            // Configure an instrument.
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
                instrumentConfiguration(&synthParams.instruments[k - '1']);
                break;

            // Tab editing.
            case SDLK_RETURN:
                tabEditor(tabs[trackSections[cursorTrackPosition].instrumentTabs[cursorInstrument]], cursorInstrument);

                // Redraw the previous screen.
                break;

            // Cursor back.
            case SDLK_LEFT:
                // Switch to the previous page?
                if (cursorPosition > 0) {
                    cursorPosition--;
                } else if (page > 0) {
                    cursorPosition = ARRANGEMENT_COLUMNS - 1;
                    page--;
                }

                break;

            // Cursor forward.
            case SDLK_RIGHT:
                // Check whether the cursor can be moved (or go to the next page).
                if (cursorPosition < ARRANGEMENT_COLUMNS - 1 && cursorTrackPosition < trackLength - 1) {
                    cursorPosition++;
                } else if (page < pageCount - 1) {
                    cursorPosition = 0;
                    page++;
                }

                break;

            // Previous instrument.
            case SDLK_UP:
                if (cursorInstrument > 0)
                    cursorInstrument--;
                break;

            // Next instrument.
            case SDLK_DOWN:
                if (cursorInstrument < SYNTH_INSTRUMENT_COUNT - 1)
                    cursorInstrument++;

                break;

            // Preview current tab.
            case 'p':
                playFullTab(cursorPosition);
                break;

            // Full preview (TODO allow the user to interrupt this).
            case '`':
                for (i = 0; i < trackLength; i++) {
                    // Draw progress.
                    arrangementDraw(cursorInstrument, cursorPosition, i, i / ARRANGEMENT_COLUMNS, pageCount);

                    // Play a tab, wait in the background before scheduling a new one.
                    playFullTab(i);
                    SDL_Delay(60000 * synthParams.tabSize / synthParams.tempo);
                }
                break;

            // Escape can be pressed to quit the application.
            case SDLK_ESCAPE:
                drawString(framebuf, 1, 0, COLOR_BLUE, COLOR_YELLOW, "Exiting; save changes? (y/n/cancel)");
                redraw();

                finished = 0;
                while (!finished) {
                    switch (waitKey()) {
                        case 'y':
                            exportFile();
                            return;

                        case 'n':
                            return;

                        default:
                            finished = 1;
                    }
                }

                // Redraw the previous screen.
                break;

            // Show help.
            case SDLK_F1:
                arrangementHelp();
                break;

            // Spacebar allows setting the tab index of a given cell.
            case ' ':
                // If the user hasn't cancelled the input, assign the new value.
                if ((tabIndex = promptTabIndex()) != -1)
                    trackSections[cursorTrackPosition].instrumentTabs[cursorInstrument] = tabIndex;
                break;

            // To the previous page.
            case '[':
                if (page > 0) {
                    page--;
                    cursorPosition = 0;
                }
                break;

            // To the next page.
            case ']':
                if (page < pageCount - 1) {
                    page++;
                    cursorPosition = 0;
                }
                break;

            // Change the track length.
            case '=':
                trackLength = promptNumber(1, MAX_TRACK_LENGTH);

                // Go to the beginning in case the track shortens.
                page = 0;
                cursorPosition = 0;
                break;

            // Move tabs left.
            case ';':
                // We can't do that on the very end of the track.
                if (cursorTrackPosition == trackLength - 1)
                    break;

                // Copy to the left.
                for (i = 0; i < trackLength; i++)
                    trackSections[i] = trackSections[i + 1];

                // Clear out the last one.
                for (i = 0; i < SYNTH_INSTRUMENT_COUNT; i++)
                    trackSections[trackLength - 1].instrumentTabs[i] = 0;

                break;

            // Move tabs right, insert a zero section.
            case '\'':
                // Can't do this at the end.
                if (cursorTrackPosition == trackLength - 1)
                    break;

                // Copy over.
                for (i = trackLength - 1; i >= cursorTrackPosition + 1; i--)
                    trackSections[i] = trackSections[i - 1];

                // Clear the new section.
                for (i = 0; i < SYNTH_INSTRUMENT_COUNT; i++)
                    trackSections[cursorTrackPosition].instrumentTabs[i] = 0;

                break;
        }
    }
}

// Draw the new file screen.
void newFileDraw() {
    char buf[64];

    clearScreen(framebuf, COLOR_LGREY);
    drawString(framebuf, 0, 0, COLOR_BLUE, COLOR_RED, "New file...");

    snprintf(buf, 64, "Synth tab size [z]: %d", synthParams.tabSize);
    drawString(framebuf, 2, 0, COLOR_GREY, COLOR_BLACK, buf);

    snprintf(buf, 64, "Synth tempo [x]: %d", synthParams.tempo);
    drawString(framebuf, 3, 0, COLOR_GREY, COLOR_BLACK, buf);

    redraw();
}

// The new file configuration screen.
void newFileScreen() {
    while (1) {
        newFileDraw();

        switch (waitKey()) {
            // Change tab size.
            case 'z':
                synthParams.tabSize = promptNumber(1, 64);
                break;

            // Change tempo.
            case 'x':
                synthParams.tempo = promptNumber(120, 1920);
                break;

            // Return.
            case SDLK_RETURN:
                return;
        }
    }
}

// Allocate track sections.
int allocateTrackSections() {
    // calloc will make all the ints zero, corresponding to the first tab.
    if (!(trackSections = calloc(MAX_TRACK_LENGTH, sizeof(track_section_t))))
        return 0;

    return 1;
}

// Create a new file.
void createNewFile() {
    int i;

    // Initialize the synth/instrument parameters to be edited later.
    synthParams.tabSize = 8;
    synthParams.tempo = 120;
    trackLength = 8;

    // Initialize instrument parameters to something meaningful.
    for (i = 0; i < SYNTH_INSTRUMENT_COUNT; i++) {
        synthParams.instruments[i].oscillatorType = i;
        synthParams.instruments[i].attackDecay = 0.75f;
        synthParams.instruments[i].transitionSustain = 0.25f;
        synthParams.instruments[i].envelopeDuration = 1.0f;
        synthParams.instruments[i].volume = 0.8f;
    }

    // Display a new file screen.
    newFileScreen();

    // Allocate the tabs.
    if (!allocateTabs()) {
        perror("Couldn't allocate memory for the tabs");
        quit(EXIT_FAILURE);
    }

    // Allocate the track sections.
    if (!allocateTrackSections()) {
        perror("Couldn't allocate memory for the track sections");
        quit(EXIT_FAILURE);
    }
}

// Import the synthesizer state.
void importFile() {
    int i, x;
    FILE *f;

    if (!(f = fopen(filename, "r"))) {
        perror("Couldn't open the file for reading");
        quit(EXIT_FAILURE);
    }

    // Try reading the version.
    if (!fread(&x, sizeof(int), 1, f)) {
        perror("Couldn't read the file version number");
        fclose(f);
        quit(EXIT_FAILURE);
    }

    // Verify the version.
    if (x != XOKN_FILE_VERSION) {
        fprintf(stderr, "Invalid version number");
        fclose(f);
        quit(EXIT_FAILURE);
    }

    // Read the synth parameters.
    if (!fread(&synthParams, sizeof(synth_params_t), 1, f)) {
        perror("Couldn't read synthParams");
        fclose(f);
        quit(EXIT_FAILURE);
    }

    // Allocate the tabs.
    if (!allocateTabs()) {
        perror("Couldn't allocate memory for the tabs");
        fclose(f);
        quit(EXIT_FAILURE);
    }

    // Read the tabs.
    for (i = 0; i < TAB_COUNT; i++) {
        if (fread(tabs[i], sizeof(synth_note_t), synthParams.tabSize, f) != (unsigned int)synthParams.tabSize) {
            perror("Couldn't read tab data");
            fclose(f);
            quit(EXIT_FAILURE);
        }
    }

    // Read the track sections.
    if (!fread(&trackLength, sizeof(int), 1, f)) {
        perror("Couldn't read the track length");
        fclose(f);
        quit(EXIT_FAILURE);
    }

    // Allocate the memory for track sections.
    if (!allocateTrackSections()) {
        perror("Couldn't allocate memory for the track sections");
        fclose(f);
        quit(EXIT_FAILURE);
    }

    // Read the track sections.
    for (i = 0; i < trackLength; i++) {
        if (!fread(&trackSections[i], sizeof(track_section_t), 1, f)) {
            perror("Couldn't read track section data");
            fclose(f);
            quit(EXIT_FAILURE);
        }
    }

    // Close the file.
    fclose(f);
}


// Show an export parameter screen.
void exportParameterScreen() {
    char c;

    clearScreen(framebuf, COLOR_LGREY);
    drawString(framebuf, 0, 0, COLOR_BLUE, COLOR_AQUA, "WAV export");
    drawString(framebuf, 2, 0, COLOR_BLUE, COLOR_GREEN, "Loop count (z/x) [0-9]: ");
    drawCharacter(framebuf, 2, 25, COLOR_BLUE, COLOR_RED, '0' + waveLoops);
    redraw();

    while (1) {
        c = waitKey();

        switch (c) {
            // z/x can be pressed to change the count of loops.
            case 'z':
            case 'x':
                if (c == 'z' && waveLoops > 1)
                    waveLoops--;
                else if (c == 'x' && waveLoops < 9)
                    waveLoops++;

                // Redraw the loop count.
                drawCharacter(framebuf, 2, 25, COLOR_BLUE, COLOR_RED, '0' + waveLoops);
                redraw();
                break;

            // Enter quits the dialog.
            case SDLK_RETURN:
                drawString(framebuf, 0, 0, COLOR_BLUE, COLOR_AQUA, "Working...");
                redraw();
                return;
        }
    }
}

// Synthesize a full tab and write it into a WAV file.
int synthAndWriteFullTab(FILE *wavfile, int trackPosition) {
    int i;
    synth_note_t *tabGroup[SYNTH_INSTRUMENT_COUNT];

    // Prepare the array of pointers to tabs for synthesizeTabGroup.
    for (i = 0; i < SYNTH_INSTRUMENT_COUNT; i++)
        tabGroup[i] = tabs[trackSections[trackPosition].instrumentTabs[i]];

    // Synthesize audio.
    synthesizeTabGroup(tabAudio, synthParams, (const synth_note_t**)tabGroup);

    // Try converting and writing it into a WAV file.
    return waveConvertAndWriteSamples(wavfile, tabAudio, tabDuration(synthParams));
}


// Export a WAV file.
void exportWAV() {
    int loop, i, samplesPerTab, totalFrames;
    FILE *wavfile;
    wave_header_t waveHeader;

    // Compute the total length of audio produced.
    samplesPerTab = tempoToSamples(synthParams.tempo) * synthParams.tabSize;
    totalFrames = samplesPerTab * trackLength * waveLoops;

    // Initialize the WAV header.
    waveInitHeader(&waveHeader, totalFrames);

    // Try opening the file.
    if (!(wavfile = fopen(exportFilename, "w"))) {
        perror("Couldn't open the output WAV file for writing");
        quit(EXIT_FAILURE);
    }

    // Try writing the header.
    if (!fwrite(&waveHeader, sizeof(wave_header_t), 1, wavfile)) {
        perror("Couldn't write the WAV file header");
        quit(EXIT_FAILURE);
    }

    // Produce audio.
    for (loop = 0; loop < waveLoops; loop++) {
        for (i = 0; i < trackLength; i++) {
            if (!synthAndWriteFullTab(wavfile, i)) {
                perror("Couldn't write WAV samples");
                quit(EXIT_FAILURE);
            }
        }
    }
}

int main(int argc, char *argv[]) {
    struct stat statbuf;

    // Zero the pointers out so that free can be used even if one of the mallocs fails before they'd be initialized.
    framebuf = NULL;
    previewAudio = NULL;
    tabAudio = NULL;
    trackSections = NULL;
    tabsAllocated = 0;

    // No export filename set.
    exportFilename = NULL;

    // Set the default octave offset for the note picker.
    octaveOffset = 4;

    // The count of WAV export loops.
    waveLoops = 1;

    // The keyboard offset.
    keyOffset = 0;

    // xokn <file.xokn> <outfile.wav>
    if (argc == 3) {
        exportFilename = argv[2];
    } else if (argc != 2) {
        fprintf(stderr, "Usage: %s <file.xokn> [outfile.wav]\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Set the filename pointer.
    filename = argv[1];

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
        fprintf(stderr, "SDL_Init failed: %s\n", SDL_GetError());
        return EXIT_FAILURE;
    }

    if (SDL_CreateWindowAndRenderer(SCREEN_WIDTH, SCREEN_HEIGHT, 0, &window, &renderer) < 0) {
        fprintf(stderr, "SDL_CreateWindowAndRenderer failed: %s\n", SDL_GetError());
        SDL_Quit();
        return EXIT_FAILURE;
    }

    if (!(texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBX8888, SDL_TEXTUREACCESS_STREAMING, SCREEN_WIDTH, SCREEN_HEIGHT))) {
        fprintf(stderr, "SDL_CreateTexture failed: %s\n", SDL_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        return EXIT_FAILURE;
    }

    // Try opening the audio device.
    SDL_zero(audioSpec);
    audioSpec.freq = SYNTH_SAMPLE_RATE;
    audioSpec.format = AUDIO_F32;
    audioSpec.channels = 1;

    if (!(audioDevice = SDL_OpenAudioDevice(NULL, 0, &audioSpec, NULL, 0))) {
        fprintf(stderr, "SDL_OpenAudioDevice failed: %s\n", SDL_GetError());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        return EXIT_FAILURE;
    }

    // Unpause the audio device.
    SDL_PauseAudioDevice(audioDevice, 0);

    // All of the SDL stuff has been initialized at this point, it's safe to use quit.
    // Try allocating the frame buffer.
    if (!(framebuf = malloc(SCREEN_WIDTH * SCREEN_HEIGHT * sizeof(pixel_t)))) {
        perror("Couldn't allocate the frame buffer");
        quit(EXIT_FAILURE);
    }

    // Try allocating the preview audio buffer.
    if (!(previewAudio = malloc(PREVIEW_DURATION * sizeof(float)))) {
        perror("Couldn't allocate the preview audio buffer");
        quit(EXIT_FAILURE);
    }

    // Set the window title.
    SDL_SetWindowTitle(window, "xokn");

    // Try allocating the resources.

    // Check if the file exists using stat and act accordingly.
    if (stat(filename, &statbuf) != 0) {
        // We can only create a new file if the export filename isn't given.
        if (!exportFilename) {
            createNewFile();
        } else {
            fprintf(stderr, "The project file for exporting as WAV doesn't exist.\n");
            quit(EXIT_FAILURE);
        }
    } else {
        importFile();
    }

    if (!(tabAudio = allocateSampleBuffer(synthParams))) {
        perror("Couldn't allocate memory for the sample buffer");
        quit(EXIT_FAILURE);
    }

    // If there's no export filename, display the arrangement screen, otherwise try exporting a WAV.
    if (!exportFilename)
        arrangementScreen();
    else {
        exportParameterScreen();
        exportWAV();
    }

    quit(EXIT_SUCCESS);
}
