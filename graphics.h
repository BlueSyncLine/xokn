#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <stdint.h>
#include "font.h"

#define CHARACTER_WIDTH (FONT_WIDTH + 1)
#define CHARACTER_HEIGHT (FONT_HEIGHT + 1)
#define SCREEN_COLS 80
#define SCREEN_ROWS 25
#define SCREEN_WIDTH (SCREEN_COLS * CHARACTER_WIDTH)
#define SCREEN_HEIGHT (SCREEN_ROWS * CHARACTER_HEIGHT)

typedef uint32_t pixel_t;
#define RGBX(r, g, b) ((r)<<24 | (g) << 16 | b << 8)
#define COLOR_BLACK  RGBX(0, 0, 0)
#define COLOR_WHITE  RGBX(255, 255, 255)
#define COLOR_GREY   RGBX(128, 128, 128)
#define COLOR_LGREY  RGBX(160, 160, 160)

#define COLOR_RED    RGBX(255, 0, 0)
#define COLOR_GREEN  RGBX(0, 255, 0)
#define COLOR_BLUE   RGBX(0, 0, 255)
#define COLOR_YELLOW RGBX(255, 255, 0)
#define COLOR_PURPLE RGBX(255, 0, 255)
#define COLOR_AQUA   RGBX(0, 255, 255)

void clearScreen(pixel_t *framebuf, pixel_t color);
void putPixel(pixel_t *framebuf, int x, int y, pixel_t color);
void drawLine(pixel_t *framebuf, int x1, int y1, int x2, int y2, pixel_t color);
void fillRect(pixel_t *framebuf, int x, int y, int w, int h, pixel_t color);
void drawCharacter(pixel_t *framebuf, int row, int col, pixel_t bg, pixel_t fg, char c);
void drawString(pixel_t *framebuf, int row, int col, pixel_t bg, pixel_t fg, const char *s);
#endif
