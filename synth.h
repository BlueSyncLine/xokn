#ifndef SYNTH_H
#define SYNTH_H

#include <stdint.h>

enum oscillators {
    OSCILLATOR_SINE,
    OSCILLATOR_SQUARE,
    OSCILLATOR_SAWTOOTH,
    OSCILLATOR_TRIANGLE,
    OSCILLATOR_SINEMOD,
    OSCILLATOR_SQUARELIKE,
    OSCILLATOR_NOISE,
    OSCILLATOR_EFFECT,
    OSCILLATOR_WOBBLE_1,
    OSCILLATOR_WOBBLE_2,
    OSCILLATOR_POWER,
    OSCILLATOR_COUNT
};

// Reference frequency for C0, 440.0 * 2**(-57/12.0)
#define SYNTH_C0 16.351597831287414f

// The index of A4.
#define TONE_A4 114

// Maximum chord size.
#define SYNTH_CHORD_SIZE 4

// This is a 24-TET synthesizer.
#define OCTAVE_SIZE 24

// The standard sample rate of 44100 Hz is used.
#define SYNTH_SAMPLE_RATE 44100

// The count of instruments synthesized.
#define SYNTH_INSTRUMENT_COUNT 8

extern const char *OSCILLATOR_NAMES[];
extern const char *NOTE_NAMES[];

typedef struct {
    int oscillatorType;
    float attackDecay;
    float transitionSustain;
    float envelopeDuration;
    float volume;
} instrument_params_t;

typedef struct {
    int tones[SYNTH_CHORD_SIZE];
    int durationSteps;
} synth_note_t;

typedef struct {
    instrument_params_t instruments[SYNTH_INSTRUMENT_COUNT];
    int tabSize;
    int tempo;
} synth_params_t;

float toneToFrequency(int tone);
int tempoToSamples(int stepsPerMinute);

float oscillatorFunction(int oscillatorType, float frequency, int t);
float envelope(float attackDecay, float transitionSustain, float envelopeDuration, float t);

float *allocateSampleBuffer(synth_params_t synthParams);
void synthesizeTabGroup(float *samples, synth_params_t synthParams, const synth_note_t *tabs[SYNTH_INSTRUMENT_COUNT]);

int tabDuration(synth_params_t synthParams);
#endif
