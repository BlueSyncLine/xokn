#include <stdlib.h>
#include <malloc.h>

#include <math.h>
#include "synth.h"

// The names of 24-TET notes.
const char *NOTE_NAMES[OCTAVE_SIZE] = {
    "C", "C+", "C#", "Dd",
    "D", "D+", "D#", "Ed",
    "E", "Fd",
    "F", "F+", "F#", "Gd",
    "G", "G+", "G#", "Ad",
    "A", "A+", "A#", "Bd",
    "B", "B+"
};


// The names for every oscillator.
const char *OSCILLATOR_NAMES[OSCILLATOR_COUNT] = {
    "sine",
    "square",
    "sawtooth",
    "triangle",
    "sinemod",
    "squarelike",
    "noise",
    "effect",
    "wobble 1",
    "wobble 2",
    "power"
};

// Computes the value of every oscillator function for a given x.
float oscillatorFunction(int oscillatorType, float frequency, int t) {
    float x, p;

    x = frequency * t * M_PI * 2.0f / SYNTH_SAMPLE_RATE;
    p = fmod(x, M_PI * 2.0f) / (M_PI * 2.0f);

    switch (oscillatorType) {
        case OSCILLATOR_SQUARE:
            return p >= 0.5f ? 1.0f : -1.0f;
        case OSCILLATOR_SAWTOOTH:
            return p * 2.0f - 1.0f;
        case OSCILLATOR_TRIANGLE:
            return p < 0.5f ? (p * 4.0f - 1.0f) : (1.0f - (p - 0.5f) * 4.0f);
        case OSCILLATOR_SINEMOD:
            return sinf(x + sinf(x));
        case OSCILLATOR_SQUARELIKE:
            return sinf(x + sinf(x * 2.0f));
        case OSCILLATOR_NOISE:
            return (rand() & 1) * 2.0f - 1.0f;
        case OSCILLATOR_EFFECT:
            return sinf(x - 0.5f * cosf(x * 3.0f));
        case OSCILLATOR_WOBBLE_1:
            return sinf(x - cosf(x * 2.0f) * sinf(20.0f * t * M_PI * 2.0 / SYNTH_SAMPLE_RATE));
        case OSCILLATOR_WOBBLE_2:
            return sinf(x - cosf(x * 1.5f) * (1.0f + 0.25f * sinf(7.5f * t * M_PI * 2.0 / SYNTH_SAMPLE_RATE)));
        case OSCILLATOR_POWER:
            return powf((sinf(x) + sinf(x * 1.5f)) / 2.0f, 3.0f);
        default:
            return sinf(x);
    }
}

// Computes the value of the attack-sustain-decay envelope.
float envelope(float attackDecay, float transitionSustain, float envelopeDuration, float t) {
    float sustain, attack, decay, envelopeT;

    sustain = 1.0f - transitionSustain;
    attack = transitionSustain * attackDecay;
    decay = transitionSustain - attack;

    // Find how deep into the envelope we are.
    if (envelopeDuration > 0.0f) {
        envelopeT = t / envelopeDuration;

        if (envelopeT > 1.0f)
            envelopeT = 1.0f;
    } else {
        // Do not divide by zero.
        envelopeT = 0.0f;
    }

    // Attack
    if (envelopeT < attack)
        return envelopeT / attack;

    // Sustain
    else if (envelopeT >= attack && envelopeT < attack + sustain)
        return 1.0f;

    // Decay
    else
        return 1.0f - (envelopeT - (attack + sustain)) / decay;
}

// Computes the frequency of a given tone.
float toneToFrequency(int tone) {
    // Compute the equal-temperament frequency for a given tone.
    return SYNTH_C0 * powf(2.0f, tone / (float)OCTAVE_SIZE);
}

// Converts a tempo value to a count of samples.
int tempoToSamples(int stepsPerMinute) {
    return SYNTH_SAMPLE_RATE * 60 / stepsPerMinute;
}

// Clamps a floating-point value to the range of -1, 1.
float clamp(float x) {
    if (x < -1.0f)
        return -1.0f;

    else if (x > 1.0f)
        return 1.0f;

    return x;
}

// Allocates a buffer for synthesizeTabGroup.
float *allocateSampleBuffer(synth_params_t synthParams) {
    return calloc(tempoToSamples(synthParams.tempo) * synthParams.tabSize, sizeof(float));
}

// Synthesizes a group of tabs.
void synthesizeTabGroup(float *samples, synth_params_t synthParams, const synth_note_t *tabs[SYNTH_INSTRUMENT_COUNT]) {
    int i, j, k, u, t, tone;
    int samplesPerStep;
    float mixSample, envelopeAmplitude, frequency;

    // The current instrument.
    instrument_params_t currentInstrument;

    // The notes currently played by the instruments.
    synth_note_t currentNotes[SYNTH_INSTRUMENT_COUNT];

    // The samples remaining until the next note for every instrument.
    int samplesUntilNextNote[SYNTH_INSTRUMENT_COUNT];

    // Initialize the array.
    for (i = 0; i < SYNTH_INSTRUMENT_COUNT; i++)
        samplesUntilNextNote[i] = 0;

    // Find the count of samples per a tempo step.
    samplesPerStep = tempoToSamples(synthParams.tempo);

    // Samples so far.
    t = 0;

    // Iterate through the tabs.
    for (i = 0; i < synthParams.tabSize; i++) {
        // The duration of a step.
        for (j = 0; j < samplesPerStep; j++) {
            // The mixed sample.
            mixSample = 0.0f;

            // Iterate through the instruments.
            for (k = 0; k < SYNTH_INSTRUMENT_COUNT; k++) {
                // Get the parameters for the current instrument.
                currentInstrument = synthParams.instruments[k];

                // Next note?
                if (samplesUntilNextNote[k] == 0) {
                    // Get the current note of the current instrument.
                    currentNotes[k] = tabs[k][i];
                    samplesUntilNextNote[k] = currentNotes[k].durationSteps * samplesPerStep;
                }

                // Compute the current envelope amplitude (divided by the chord size).
                envelopeAmplitude = envelope(currentInstrument.attackDecay, currentInstrument.transitionSustain, currentInstrument.envelopeDuration, 1.0f - (float)samplesUntilNextNote[k] / (currentNotes[k].durationSteps * samplesPerStep)) / SYNTH_CHORD_SIZE * currentInstrument.volume;

                // Synthesize the tones of the chord.
                for (u = 0; u < SYNTH_CHORD_SIZE; u++) {
                    // Is the note active?
                    if ((tone = currentNotes[k].tones[u]) >= 0) {
                        frequency = toneToFrequency(tone);
                        mixSample += envelopeAmplitude * oscillatorFunction(currentInstrument.oscillatorType, frequency, t);
                    }
                }

                // Decrement the counter.
                samplesUntilNextNote[k]--;
            }

            // Store the calculated sample.
            samples[t++] = clamp(mixSample);
        }
    }
}

// Compute the duration of one tab in samples from the synth parameters.
int tabDuration(synth_params_t synthParams) {
    return tempoToSamples(synthParams.tempo) * synthParams.tabSize;
}
